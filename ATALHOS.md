## Timeline


Atalho | Descrição
------------ | -------------
**Space** | Avançar e parar 
**H** | Ferramenta Mão
**V** | Ferramenta Selecionar
**C** | Ferramenta Cortar
**L** | Play (apertando novamente aumenta a velocidade)
**K** | Stop 
**J** | Retrocede (apertando novamente volta em x2)
**◀️ ▶️** | Avança ou Retrocede Frame a Frame
**+ ou -** | Zoom In e Zoom Out na Timeline
**Shift + ◀️ ou ▶️** | Avança ou retrocede 5 segundos
**Shift + ou -** | Aumenta ou Volta ao normal o espaço do Zoom da Track
**CTRL + ou -** | Aumenta só a Track de Vídeo
**ALT + ou -** |  Aumenta só a Track de Áudio
**🔽 ou 🔼** | Anda entre os cortes
**CTRL + K** | Realiza um corte (Se atente a seleção das Tracks)


### Project 

Atalho | Descrição
------------ | -------------
**Arrastar o Vídeo + CTRL** | para colocar o Vídeo entre o corte


### Essentials 

Atalho | Descrição
------------ | -------------
**I e O** | Adiciona pontos de entrada e saída
**D** | é o atalho no teclado para selecionar o clipe em Playhead
**K** | Para a linha do tempo
**Barra invertida ( \ )** | Serve para dar zoom na sequência e mostrar tudo na linha do tempo
**Shift + Ctrl + A** | Para desfazer a seleção de todos os clipes, efeitos ou qualquer item que esteja selecionado.
**F** | é para Match Frame. Pare sobre qualquer clipe na sequência, pressione F para abrir o clipe principal no quadro exato no monitor de origem.
**Shift + = / –** | Expande e minimiza as faixas da linha do tempo. Com Ctrl + = / – (Cmd + = / –) controle só a altura da faixa de vídeo e com Alt + = / – (Opt + = / –) só a altura da faixa de áudio.
**I e O** | Adiciona pontos de entrada e saída. Apague os pontos de entrada e saída com Ctrl + Shift + I (Opt + I ), Ctrl + Shift + O (Opt + O) e Ctrl + Shift + X (Opt + X).
**Shift + Ctrl + A (Shift + Cmd + A)** | Para desfazer a seleção de todos os clipes, efeitos ou qualquer item que esteja selecionado.
**Ctrl + K (Cmd + K)** | permite cortar o clipe em duas faixas ativas. Acrescentando o Shift, você pode cortar todos os clipes embaixo da Playhead em dois.
**Ctrl + 0 (Cmd + 0)** | permite abrir um novo projeto. Além disso, Ctrl + N (Cmd + N) adiciona uma nova sequência.
**X** | marcará o clipe, mas a / (barra) definirá as entradas e saídas em todos os clipes que tenham sido selecionados.



